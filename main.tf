provider "aws" {
  region = var.region
  profile = var.profile
}

##
# autoScaling_delete
##
module "lambda_autoScaling_delete" {

  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-autoScaling_delete"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/autoScaling_de+lete.zip"
  function_dir            = "${path.module}/lambda_functions/autoScaling_delete/"
  json_policy             = file("${path.module}/policies/autoScaling_delete.json")
  environment_variables   = {
                              min_capacity              = tostring(var.min_capacity)
                              max_capacity              = tostring(var.max_capacity)
                              cluster_identifier        = var.cluster_identifier
                              delete_target             = var.delete_target
                              db_instance_class_default = var.db_instance_class_default
                            }
}
module "cloudWatchEventBridge_autoScaling_delete" {

  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}-autoScaling_delete"
  schedule_expression     = "cron(0/10 * * * ? *)"
  lambda_main_arn         = module.lambda_autoScaling_delete.lambda_arn
}

##
# autoScaling
##
module "lambda_autoScaling" {

  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = var.name
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/autoScaling.zip"
  function_dir            = "${path.module}/lambda_functions/autoScaling/"
  json_policy             = file("${path.module}/policies/autoScaling.json")
  environment_variables   = {
                              min_capacity       = tostring(var.min_capacity)
                              max_capacity       = tostring(var.max_capacity)
                              cluster_identifier = var.cluster_identifier
                              db_instance_class_default = var.db_instance_class_default
                              db_instance_class_reziseNight = var.db_instance_class_reziseNight
                              autoscaling_target            = var.autoscaling_target
                            }
}
module "cloudWatchEventBridge_autoScaling" {

  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}"
  schedule_expression     = "cron(0/3 * * * ? *)"
  lambda_main_arn         = module.lambda_autoScaling.lambda_arn
}

##
# minimumInstances
##
module "lambda_minimumInstances" {

  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-minimumInstances"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/minimumInstances.zip"
  function_dir            = "${path.module}/lambda_functions/minimumInstances/"
  json_policy             = file("${path.module}/policies/minimumInstances.json")
  environment_variables   = {
                              min_capacity       = tostring(var.min_capacity)
                              max_capacity       = tostring(var.max_capacity)
                              cluster_identifier = var.cluster_identifier
                              db_instance_class_default = var.db_instance_class_default
                            }
}
module "cloudWatchEventBridge_minimumInstances" {

  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}-minimumInstances"
  schedule_expression     = "cron(* * * * ? *)"
  lambda_main_arn         = module.lambda_minimumInstances.lambda_arn
}

##
# resizeNight
##
module "lambda_resizeNight" {

  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0)  ?  1 : 0
  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-resizeNight"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/resizeNight.zip"
  function_dir            = "${path.module}/lambda_functions/resizeNight/"
  json_policy             = file("${path.module}/policies/resizeNight.json")
  environment_variables   = {
                              cluster_identifier = var.cluster_identifier
                              db_instance_class_reziseNight = var.db_instance_class_reziseNight
                              resize_nigth_name_primary = var.resize_nigth_name_primary
                              resize_nigth_name_replica = var.resize_nigth_name_replica                              
                            }                        
}
module "cloudWatchEventBridge_resizeNight" {
  
  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0)  ?  1 : 0
  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}-resizeNight"
  schedule_expression     = "cron(00 22 * * ? *)"
  lambda_main_arn         = module.lambda_resizeNight[0].lambda_arn
}

##
# selfServiceNormalize
##
module "lambda_selfServiceNormalize" {

  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-selfServiceNormalize"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/selfServiceNormalize.zip"
  function_dir            = "${path.module}/lambda_functions/selfServiceNormalize/"
  json_policy             = file("${path.module}/policies/selfServiceNormalize.json")
  environment_variables   = {
                              cluster_identifier        = var.cluster_identifier
                              db_instance_class_default = var.db_instance_class_default
                              trigger_target            = var.trigger_target
                            }
}
module "cloudWatchEventBridge_selfServiceNormalize" {

  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}-selfServiceNormalize"
  schedule_expression     = "cron(*/30 * * * ? *)"
  lambda_main_arn         = module.lambda_selfServiceNormalize.lambda_arn
}

##
# resizeDay
##
module "lambda_resizeDay" {

  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0) ? 1 : 0
  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-resizeDay"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/resizeDay.zip"
  function_dir            = "${path.module}/lambda_functions/resizeDay/"
  json_policy             = file("${path.module}/policies/resizeDay.json")
  environment_variables   = {
                              cluster_identifier = var.cluster_identifier
                              db_instance_class_default = var.db_instance_class_default
                              resize_nigth_name_primary = var.resize_nigth_name_primary
                              resize_nigth_name_replica = var.resize_nigth_name_replica
                            }                        
}
module "cloudWatchEventBridge_resizeDay" {

  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0) ? 1 : 0
  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}-resizeDay"
  schedule_expression     = (length(regexall(".*production.*", terraform.workspace)) == 0) ? "cron(00 09 ? * MON-FRI *)" : "cron(30 09 ? * MON-FRI *)"
  lambda_main_arn         = module.lambda_resizeDay[0].lambda_arn
}

##
# stopCluster
##
module "lambda_stopCluster" {
  # Execution only in non-production
  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0) ? 0 : 1
  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-stopCluster"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/stopCluster.zip"
  function_dir            = "${path.module}/lambda_functions/stopCluster/"
  json_policy             = file("${path.module}/policies/stopCluster.json")
  environment_variables   = {
                              cluster_identifier = var.cluster_identifier
                            }   
}

module "cloudWatchEventBridge_stopCluster" {
  # Execution only in non-production
  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0) ? 0 : 1
  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}-stopCluster"
  schedule_expression     = "cron(00 03 * * ? *)"
  lambda_main_arn         = module.lambda_stopCluster[0].lambda_arn
}

##
# startCluster
##
module "lambda_startCluster" {
  # Execution only in non-production
  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0) ? 0 : 1
  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-startCluster"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/startCluster.zip"
  function_dir            = "${path.module}/lambda_functions/startCluster/"
  json_policy             = file("${path.module}/policies/startCluster.json")
  environment_variables   = {
                              cluster_identifier = var.cluster_identifier
                            }   
}

module "cloudWatchEventBridge_startCluster" {
  # Execution only in non-production
  count                   = (length(regexall(".*production.*", terraform.workspace)) > 0) ? 0 : 1
  source                  = "./modules/cloudwatchEventBridge"
  function_name           = "${var.name}-startCluster"
  schedule_expression     = "cron(00 09 ? * MON-FRI *)"
  lambda_main_arn         = module.lambda_startCluster[0].lambda_arn
}

##
# selfServiceInstance
##
module "lambda_selfServiceInstance" {

  source                  = "./modules/lambda"
  aws_account_id          = var.aws_account_id
  function_name           = "${var.name}-selfServiceInstance"
  handler                 = "index.handler"
  output_path             = "${path.module}/.output/selfServiceInstance.zip"
  function_dir            = "${path.module}/lambda_functions/selfServiceInstance/"
  json_policy             = file("${path.module}/policies/selfServiceInstance.json")
  timeout                 = 900
  environment_variables   = {
                              cluster_identifier          = var.cluster_identifier
                              db_instance_class_default   = var.db_instance_class_default
                              trigger_target              = var.trigger_target
                              selfService_queue           = "${var.name}-selfServiceInstance-queue"
                              dead_letter_queue           = module.sqs_deadLetter_selfServiceInstance.url
                              typeInstanceClassAvailable  = "db.t3.medium, db.r6g.large, db.r6g.xlarge, db.r6g.2xlarge, db.r6g.2xlarge, db.r6g.4xlarge"
                            }
  depends_on = [
    module.sqs_deadLetter_selfServiceInstance
  ]                            
}
module "sqs_trigger_lambda_selfServiceInstance" {
  
  source                      = "./modules/sqs_trigger_lambda"
  function_name               = "${var.name}-selfServiceInstance"
  visibility_timeout_seconds  = 900

  depends_on = [
    module.lambda_selfServiceInstance
  ]
}
module "sqs_deadLetter_selfServiceInstance" {
  
  source                      = "./modules/sqs"
  name                        = "${var.name}-selfServiceInstance"
  visibility_timeout_seconds  = 30
}