# IAC - Terraform to AutoScaling AWS DocumentDB 

![See here archtecture diagram](./diagram/AutoScaling-AWS-DocumentDB-v2.jpeg)

### Setup for development

1. Install terraform cli.

2. Create a IAM user with programmatic access to AWS for Terraform to use.


### Prepare environment

- If not exists, add the correct environments inside a file named:
```
    /env/development.tfvars
    /env/staging.tfvars
    /env/production.tfvars
    
 ```

- If not exists, create of Workspace to environments:
```
    terraform workspace new development
    terraform workspace new staging
    terraform workspace new production

```


### Start Terraform
 ```
    terraform init
```
### Destroy to AWS

- Destroy:
 ```
    terraform workspace select development && terraform destroy -var-file=env/development.tfvars
    terraform workspace select staging && terraform destroy -var-file=env/staging.tfvars
    terraform workspace select production && terraform destroy -var-file=env/production.tfvars

 ```

### Plan and Deploy to AWS

- See the plan:
 ```
    terraform workspace select development && terraform plan -var-file=env/development.tfvars
    terraform workspace select staging && terraform plan -var-file=env/staging.tfvars
    terraform workspace select production && terraform plan -var-file=env/production.tfvars

 ```

- Apply the infrastructure:

```
    terraform workspace select development && terraform apply -var-file=env/development.tfvars -parallelism=3
    terraform workspace select staging && terraform apply -var-file=env/staging.tfvars -parallelism=3
    terraform workspace select production && terraform apply -var-file=env/production.tfvars -parallelism=3
    
```

#### Architect and developer: [Marcelo Viana](https://www.linkedin.com/in/marcelovianaalmeida/)

This project is a fork of the https://github.com/theuves/docdb-autoscaling