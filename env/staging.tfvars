profile                       = "default"
region                        = "us-east-1"
aws_account_id                = "000000000000"
cluster_identifier            = "project_name-staging"
name                          = "docdb-autoscaling-staging"
db_instance_class_default     = "db.r6g.large"
db_instance_class_reziseNight = "db.t3.medium"
resize_nigth_name_primary     = "lambda-resizeNight-primary"
resize_nigth_name_replica     = "lambda-resizeNight-replica"
trigger_target                = 50
autoscaling_target            = 70

scaling_policy = [
    {
      metric_name = "CPUUtilization"
      target          = 90
      statistic       = "Average"
      cooldown        = 300
    },   
    {
      metric_name = "DatabaseConnections"
      target          = 400
      statistic       = "Average"
      cooldown        = 30
    }    

  ]
