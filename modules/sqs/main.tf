resource "aws_sqs_queue" "this" {
  name = "${var.name}-dead-letter-queue"
  visibility_timeout_seconds  = var.visibility_timeout_seconds
}

data "aws_iam_policy_document" "sqs_queue_policy" {
  statement {
        sid      = "Stmt1679567881705"
        effect       = "Allow"
        principals {
          type        = "AWS"
          identifiers = ["*"]
        }
        actions       = ["sqs:*"]
        resources       = [aws_sqs_queue.this.arn]
  }
}

resource "aws_sqs_queue_policy" "default" {
  queue_url = aws_sqs_queue.this.id
  policy    = data.aws_iam_policy_document.sqs_queue_policy.json
}