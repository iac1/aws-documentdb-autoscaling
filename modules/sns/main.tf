resource "aws_lambda_permission" "sns" {
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_main_arn
  principal     = "sns.amazonaws.com"
  statement_id  = "AllowSubscriptionToSNS"
  source_arn    = aws_sns_topic.main.arn
}

# SNS topic (trigged by CloudWatch)
resource "aws_sns_topic" "main" {
  name = var.function_name
}

# Call AWS Lambda when publish messages
resource "aws_sns_topic_subscription" "main" {
  topic_arn = aws_sns_topic.main.arn
  protocol  = "lambda"
  endpoint  = var.lambda_main_arn
}
