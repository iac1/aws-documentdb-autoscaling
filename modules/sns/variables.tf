variable "function_name" {
  type          = string
}

variable "lambda_main_arn" {
  type          = string
}