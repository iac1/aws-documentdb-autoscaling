variable "function_name" {
  type          = string
}

variable "handler" {
  type          = string
  default       = "index.handler"
}

# variable "cluster_identifier" {
#   type          = string
# }

variable "output_path" {
  type          = string
}

variable "function_dir" {
  type          = string
  description = "Code directory of lambda function"
}

variable "environment_variables" {
  type          = any
  description   = "environment variables to lambda function"
}

variable "json_policy" {
  type          = any
  description = "attachment json policy to lambda function"
}

variable "timeout" {
  type          = number
  default       = 900
  description   = "lambda function timeout"
}

variable "aws_account_id" {
  type          = string
}