# Scale-out alarm
resource "aws_cloudwatch_metric_alarm" "cloudWatch_main" {
  count = length(var.scaling_policy)

  alarm_name          = "${var.function_name}-${count.index}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  namespace           = "AWS/DocDB"
  metric_name         = var.scaling_policy[count.index].metric_name
  statistic           = var.scaling_policy[count.index].statistic
  period              = tostring(var.scaling_policy[count.index].cooldown)
  threshold           = tostring(var.scaling_policy[count.index].target)

  # Actions
  actions_enabled = "true"
  alarm_actions   = [var.sns_topic_arn] # this action will create a new machine.
  ok_actions      = [var.sns_topic_arn] # this action will delete a machine.

  dimensions = {
    DBClusterIdentifier = var.cluster_identifier
  }
}