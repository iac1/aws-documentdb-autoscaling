variable "function_name" {
  type          = string
}

variable "scaling_policy" {
  type           = list(any)
}

variable "cluster_identifier" {
  type          = string
}

variable "sns_topic_arn" {
  type          = string
}