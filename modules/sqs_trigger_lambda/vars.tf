variable "function_name" {
  type        = string
  description = "Name the Lamda function"
}

variable "visibility_timeout_seconds" {
  default      = 30
  type         = number
}
