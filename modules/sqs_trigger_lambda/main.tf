resource "aws_sqs_queue" "this" {
  name = "${var.function_name}-queue"
  visibility_timeout_seconds  = var.visibility_timeout_seconds
}

resource "aws_lambda_event_source_mapping" "this" {
  event_source_arn = aws_sqs_queue.this.arn
  enabled          = true
  function_name    = var.function_name
  batch_size       = 1

  depends_on = [
    aws_sqs_queue.this
  ]

}

data "aws_iam_policy_document" "sqs_queue_policy" {
  statement {
        sid      = "Stmt1679567881705"
        effect       = "Allow"
        principals {
          type        = "AWS"
          identifiers = ["*"]
        }
        actions       = ["sqs:*"]
        resources       = [aws_sqs_queue.this.arn]
  }
}

resource "aws_sqs_queue_policy" "default" {
  queue_url = aws_sqs_queue.this.id
  policy    = data.aws_iam_policy_document.sqs_queue_policy.json
}