variable "function_name" {
  type          = string
}

variable "schedule_expression" {
  type           = string
}

variable "lambda_main_arn" {
  type          = string
}