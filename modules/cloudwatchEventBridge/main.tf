#Eventbridge create
resource "aws_cloudwatch_event_rule" "event_role" {
    name = var.function_name
    schedule_expression = var.schedule_expression
}

resource "aws_lambda_permission" "allow_cloudwatch_to_invoke" {
  function_name = aws_cloudwatch_event_rule.event_role.name
  statement_id = "CloudWatchInvoke"
  action = "lambda:InvokeFunction"

  source_arn = aws_cloudwatch_event_rule.event_role.arn
  principal = "events.amazonaws.com"
}

resource "aws_cloudwatch_event_target" "invoke_lambda" {
  rule = aws_cloudwatch_event_rule.event_role.name
  # arn = aws_lambda_function.lambda_main.arn
  arn = var.lambda_main_arn
}
