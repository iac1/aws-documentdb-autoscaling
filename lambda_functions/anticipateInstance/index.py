import os
import logging, boto3
import uuid

client = boto3.client('docdb')

# Environment variables
max_replicas_count = 2
min_capacity = int(os.environ.get("min_capacity"))
max_capacity = int(os.environ.get("max_capacity"))
cluster_identifier = os.environ.get("cluster_identifier")
db_instance_class_default = os.environ.get("db_instance_class_default")
instance_name_identify = "lambda"

def create(event, context):

  for index in range(max_replicas_count):
    client.create_db_instance(
      DBClusterIdentifier=cluster_identifier,
      DBInstanceIdentifier="%s-%s-%s" % (cluster_identifier, instance_name_identify, index),
      DBInstanceClass=db_instance_class_default,
      Engine="docdb",
      EnablePerformanceInsights=True    
    )

def delete(event, context):

  for index in range(max_replicas_count):
    client.delete_db_instance(
      DBInstanceIdentifier="%s-%s-%s" % (cluster_identifier, instance_name_identify, index)
    )    
  

  

