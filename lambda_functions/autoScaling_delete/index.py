import boto3, math, statistics, os
from datetime import datetime, timedelta
import libs.autoscalingclass.autoscaling as autoscaling

min_capacity = int(os.environ.get("min_capacity"))
max_capacity = int(os.environ.get("max_capacity"))
cluster_identifier = os.environ.get("cluster_identifier")
delete_target = int(os.environ.get("delete_target"))
db_instance_class_default = os.environ.get("db_instance_class_default")

docdb = autoscaling.DocumentDB(cluster_identifier, min_capacity, max_capacity, db_instance_class_default)

def handler(event, context):
    print('')
    print('Running autoScaling_delete...')
    
    client = boto3.client('cloudwatch')
    avarage = []
    cpuMetric = None
    statistics_type = "Average"

    response = client.get_metric_statistics(
        Namespace='AWS/DocDB',
        MetricName='CPUUtilization',
        Dimensions=[
            {
                'Name': 'DBClusterIdentifier',
                'Value': cluster_identifier
            },
        ],
        StartTime=datetime.utcnow() - timedelta(minutes=5),
        EndTime=datetime.utcnow(),
        Period=300,
        Statistics=[
            statistics_type,
        ],
        Unit='Percent'
    )

    if len(response['Datapoints']) == 0:
        print('No data list')
        return True

    # print(response['Datapoints'])
    for point in response['Datapoints']:
        avarage.append(math.ceil((point[statistics_type])))
        print("Current Percent CPU: ", point, "%")

    # the current value will be rounded up
    cpuMetric=statistics.mean(avarage)
    if cpuMetric < delete_target:
        docdb.remove_replica()
        return True

    print('Nothing to do!')

# Only local development
if os.environ.get("DEBUG_AUTOSCALING"):
  handler('', '')
    