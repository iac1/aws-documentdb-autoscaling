import os
from libs.horizontalScale import HorizontalScale
from libs.verticalScale import VerticalScale
from utils.snsExtract import SnsExtract

# Constants
TYPE_PROVISIONING=['horizontal', 'vertical']

# Environment variables
cluster_identifier              = os.environ.get("cluster_identifier")
db_instance_class_default       = os.environ.get("db_instance_class_default")

# declaration instances
horizontalScale         = HorizontalScale()
verticalScale           = VerticalScale()
snsExtract              = SnsExtract()

def handler(event, context):

  payloadMessage = {}
  # try to get the message from SQS trigger.
  if os.environ.get("DEBUG_AUTOSCALING"):
    print('...DEBUG_AUTOSCALING...')
    payloadMessage['payload'] = {}
    payloadMessage['replyQueueName'] = "docdb-autoscaling-development-selfServiceInstance-dead-letter-queue"
    payloadMessage['provisioningLevel'] = TYPE_PROVISIONING[1]
  else:
    payloadMessage = snsExtract.getMessage(event)

  
  if payloadMessage['replyQueueName']:
    # select autoScaling type
    if payloadMessage['provisioningLevel'] == TYPE_PROVISIONING[1]:
        verticalScale.scaleUp(payloadMessage['replyQueueName'], payloadMessage['payload'])
    elif payloadMessage['provisioningLevel'] == TYPE_PROVISIONING[0]:
        horizontalScale.scaleOut(payloadMessage['replyQueueName'], payloadMessage['payload'])


# Only local development
if os.environ.get("DEBUG_AUTOSCALING"):
  handler('', '')        