import os, json
from libs.sqs import SQS
from utils.sqsPrepareMessage import SqsPrepareMessage

dead_letter_queue              = os.environ.get("dead_letter_queue")
selfService_queue              = os.environ.get("selfService_queue")
sqs                            = SQS()
sqs_message                    = SqsPrepareMessage()

class SnsExtract:

    def getMessage(self, event):
        payload = {}
        replyQueueName = None
        # try to get the message from SQS trigger.
        try:
            for record in event['Records']:
                payload = json.loads((record["body"]))
                # response to daead letter if not available in the replyQueueName
                replyQueueName = payload['replyQueueName'] or dead_letter_queue
                receipt_handle = record['receiptHandle']
                sqs.delete(selfService_queue, receipt_handle)
        except Exception as error:
            print(error)
            pass

        if not replyQueueName:
            payload['selfServiceInstance'] =  sqs_message.prepare("Nothing to do! undefined replyQueueName", "Stop process")
            # response to backend
            sqs.send(dead_letter_queue, payload)
            return {
                    "replyQueueName": None
            }
        
        return payload
