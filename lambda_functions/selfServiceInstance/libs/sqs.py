import boto3
sqs = boto3.client('sqs')

class SQS:

    def send(self, sqs_queue_url, payload):
        print("SQS-Send was called!")
        response = sqs.send_message(
            QueueUrl=sqs_queue_url,
            DelaySeconds=10,
            MessageBody=(str(payload))
        )
        return response['MessageId']

    def delete(self, queue_url, receipt_handle):
        print("SQS-Delete was called")
        return sqs.delete_message(
            QueueUrl=queue_url,
            ReceiptHandle=receipt_handle
        )        