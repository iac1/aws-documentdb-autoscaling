import os, time
from libs.documentdb import Documentdb
from libs.getMetrics import GetMetrics
from libs.sqs import SQS
from utils.sqsPrepareMessage import SqsPrepareMessage

# Environment variables
cluster_identifier              = os.environ.get("cluster_identifier")
db_instance_class_default       = os.environ.get("db_instance_class_default")
trigger_target                  = int(os.environ.get("trigger_target"))
dead_letter_queue               = os.environ.get("dead_letter_queue")

# declaration instances
sqs                             = SQS()
sqs_message                     = SqsPrepareMessage()
docdb                           = Documentdb()
getMetric                       = GetMetrics()

# class
class HorizontalScale:
    
    def scaleOut(self, replyQueueName, payload):

        # Requirement to provision action
        if int(getMetric.get()) >= trigger_target:
            instanceName = docdb.create_db_instance(cluster_identifier, docdb.getCurrentInstanceClass(cluster_identifier), "selfService-horizontal")
            self.await_provisioning(instanceName)
            payload['selfServiceInstance'] = sqs_message.prepare("Your db-instance is ready!" + "instanceName: " + instanceName, "success", int(getMetric.get()), 'horizontal')
            # response to backend
            sqs.send(replyQueueName, payload)
            return
          
    def scaleIn():
        return "not implemented or implemented in another local"

    def await_provisioning(self, instanceName):
        controlLoop=True
        while controlLoop:
            list_db_instances = docdb.describe_db_instances(cluster_identifier)
            for index in list_db_instances["DBInstances"]:
                if index["DBInstanceIdentifier"] == instanceName:
                    print('Awaiting for creating and availability of instance: ', index["DBInstanceIdentifier"], 'status:', index["DBInstanceStatus"])
                    if index["DBInstanceStatus"] == "deleting":
                        controlLoop=False
                        return
                    if index["DBInstanceStatus"] == "available":
                        controlLoop=False
                        return
            time.sleep(10)