import os
from libs.verticalScaleHandler import VerticalScaleHandler
from libs.sqs import SQS
from utils.sqsPrepareMessage import SqsPrepareMessage
from libs.getMetrics import GetMetrics
from libs.documentdb import Documentdb

# Environment variables
cluster_identifier                  = os.environ.get("cluster_identifier")
typeInstanceClassAvailable          = os.environ.get("typeInstanceClassAvailable").replace(" ", "").split(",")
dead_letter_queue                   = os.environ.get("dead_letter_queue")
db_instance_class_default           = os.environ.get("db_instance_class_default")

# declaration instances
verticalScaleHandler                = VerticalScaleHandler()
sqs                                 = SQS()
sqs_message                         = SqsPrepareMessage()
docdb                               = Documentdb()
getMetric                           = GetMetrics()

class VerticalScale:
    
    def scaleUp(this, replyQueueName, payload):

        for index, typeInstance in enumerate(typeInstanceClassAvailable):

            if typeInstance == docdb.getCurrentInstanceClass(cluster_identifier):
                new_db_instance_class = typeInstanceClassAvailable[index + 1]
                if (index + 1) == len(typeInstanceClassAvailable):
                    new_db_instance_class = typeInstanceClassAvailable[index]
                    break
        
        # start the create vertical instance
        instancesName = verticalScaleHandler.handler(new_db_instance_class)
        payload['selfServiceInstance'] = sqs_message.prepare("Your db-instances are ready!" + "instancesName: " + str(instancesName), "success", int(getMetric.get()), 'vertical')
        
        # response to backend
        sqs.send(replyQueueName, payload)
        return                
    

    def scaleDown():
        return 'Not implemented'
