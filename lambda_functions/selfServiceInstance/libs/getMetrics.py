import boto3, math, statistics, os
from datetime import datetime, timedelta

cluster_identifier      = os.environ.get("cluster_identifier")
db_instance_class_default       = os.environ.get("db_instance_class_default")
class GetMetrics:

    def get(self):
        print('')

        client = boto3.client('cloudwatch')
        avarage = []
        statistics_type = "Average"

        response = client.get_metric_statistics(
            Namespace='AWS/DocDB',
            MetricName='CPUUtilization',
            Dimensions=[
                {
                    'Name': 'DBClusterIdentifier',
                    'Value': cluster_identifier
                },
            ],
            StartTime=datetime.utcnow() - timedelta(minutes=5),
            EndTime=datetime.utcnow(),
            Period=300,
            Statistics=[
                statistics_type,
            ],
            Unit='Percent'
        )

        if len(response['Datapoints']) == 0:
            print('No data list')
            return True

        # print(response['Datapoints'])
        for point in response['Datapoints']:
            avarage.append(math.ceil((point[statistics_type])))

        # the current value will be rounded up
        return statistics.mean(avarage)
