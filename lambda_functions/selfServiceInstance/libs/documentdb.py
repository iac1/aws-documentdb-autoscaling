import calendar, time
import boto3
client = boto3.client('docdb')

class Documentdb:

    def create_db_instance(self, cluster_identifier, db_instance_class_default, newInstanceName = "undefined"):
        
        instanceName = (newInstanceName +"-"+ db_instance_class_default.replace(".", "") + "-" + str(calendar.timegm(time.gmtime())))
        print('Creating a new instnace...', instanceName)
        try:
            response = client.create_db_instance(
                                                    DBClusterIdentifier=cluster_identifier,
                                                    DBInstanceIdentifier=instanceName,
                                                    DBInstanceClass=db_instance_class_default,
                                                    Engine="docdb",
                                                    EnablePerformanceInsights=True
                                                )
            return response['DBInstance']['DBInstanceIdentifier']

        except Exception as error:
            print(error)
            pass


    def describe_db_instances(self, cluster_identifier):
    
        return client.describe_db_instances(
            Filters=[{
                'Name': 'db-cluster-id',
                'Values': [ cluster_identifier ]
            }]
            )
    
    def describe_db_clusters(self, cluster_identifier):

        return client.describe_db_clusters(
            DBClusterIdentifier=cluster_identifier
        )    

    def failover_db_cluster(self, instancename, cluster_identifier):
        print("Modify Instance failover...", instancename)
        return client.failover_db_cluster(
                                            DBClusterIdentifier=cluster_identifier,
                                            TargetDBInstanceIdentifier=instancename
                                        )
    
    def getCurrentInstanceClass(this, cluster_identifier):
        primaryInstance = None
        currentInstanceClass = None

        list_db_instances = this.describe_db_clusters(cluster_identifier)
        for index in list_db_instances['DBClusters'][0]['DBClusterMembers']:
            if index['IsClusterWriter'] == True:
                primaryInstance = index['DBInstanceIdentifier']
                break

        list_db_instances = this.describe_db_instances(cluster_identifier)
        for index in list_db_instances["DBInstances"]:
            if index['DBInstanceIdentifier'] == primaryInstance:
                currentInstanceClass = index['DBInstanceClass']
            
        return currentInstanceClass

    def existsCurrentSelfService(this, cluster_identifier):
        selfServiceInstances = []
        for db_instance in this.describe_db_instances(cluster_identifier).get('DBInstances'):
            instanceName = db_instance.get('DBInstanceIdentifier')
            # Verify if exists world
            if "selfservice-vertical" in instanceName:
                selfServiceInstances.append(db_instance)
        
        if len(selfServiceInstances) > 0:
            return selfServiceInstances
        print("There aren't instances created by the selfService.")
        return []
