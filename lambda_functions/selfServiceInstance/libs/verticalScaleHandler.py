import os, time
import boto3
from libs.documentdb import Documentdb

# Environment variables
cluster_identifier = os.environ.get("cluster_identifier")

# declaration instances
client        = boto3.client('docdb')
docdb         = Documentdb()

class VerticalScaleHandler:

  def __init__(self) -> None:
    self.primaryInstance = None
    self.replicaInstance = None
    self.existedSelfServiceInCluster = False
    pass

  def handler(self, new_db_instance_class):

    # Phase 1 creation instance
    if self.validations():
      self.primaryInstance = docdb.create_db_instance(cluster_identifier, new_db_instance_class, "selfService-vertical-primary")
      self.replicaInstance = docdb.create_db_instance(cluster_identifier, new_db_instance_class, "selfService-vertical-replica")

    # Phase 2 Reader cluster for promotion primary instance
    controlLoop=True
    while controlLoop:
      list_db_instances = docdb.describe_db_instances(cluster_identifier)
      for index in list_db_instances["DBInstances"]:
        print('Awaiting for creating and availability of instance: ', self.primaryInstance)
        if index["DBInstanceIdentifier"] == self.primaryInstance and index["DBInstanceStatus"] == "available":
          if not self.existedSelfServiceInCluster:
            docdb.failover_db_cluster(self.primaryInstance, cluster_identifier)
          controlLoop=False

      time.sleep(10)
    
    return {
            "primaryInstance": self.primaryInstance,
            "replicaInstance": self.replicaInstance
    }
  
  def validations(self):

    currentInstances = docdb.existsCurrentSelfService(cluster_identifier)

    if len(currentInstances) > 0:
      for instance in currentInstances:
        if instance.get('DBInstanceStatus') == 'creating' or instance.get('DBInstanceStatus') == 'configuring-log-exports':
            if "primary" in instance.get('DBInstanceIdentifier'):
              self.primaryInstance = instance.get('DBInstanceIdentifier')
            if "replica" in instance.get('DBInstanceIdentifier'):
              self.replicaInstance = instance.get('DBInstanceIdentifier')
            self.existedSelfServiceInCluster = True
        
        if self.primaryInstance and self.replicaInstance:
          print('Use of the previous process...', self.primaryInstance, self.replicaInstance)
          return False
    return True
    