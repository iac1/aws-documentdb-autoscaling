import os
import libs.autoscalingclass.autoscaling as autoscaling

# Environment variables
min_capacity = int(os.environ.get("min_capacity"))
max_capacity = int(os.environ.get("max_capacity"))
cluster_identifier = os.environ.get("cluster_identifier")
db_instance_class_default = os.environ.get("db_instance_class_default")

def handler(event, context):

  docdb = autoscaling.DocumentDB(cluster_identifier, min_capacity, max_capacity, db_instance_class_default)
  replicas_count = docdb.get_replicas_count()

  # Add more replica instances to meet the minimum capacity
  if (replicas_count + 1) < min_capacity:
    print("WARNING: Adding more replica instances to meet the minimum capacity...")
    docdb.add_replica(ignore_status=True)
    return None

  print("WARNING: It's okay! Nothing to be done")