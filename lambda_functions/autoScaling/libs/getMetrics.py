import boto3, math, statistics, os
from datetime import datetime, timedelta

min_capacity = int(os.environ.get("min_capacity"))
max_capacity = int(os.environ.get("max_capacity"))
cluster_identifier = os.environ.get("cluster_identifier")
autoscaling_target = int(os.environ.get("autoscaling_target"))
db_instance_class_default = os.environ.get("db_instance_class_default")

class Metrics:

    def get(self):

        client = boto3.client('cloudwatch')
        avarage = []
        cpuMetric = None
        statistics_type = "Average"

        response = client.get_metric_statistics(
            Namespace='AWS/DocDB',
            MetricName='CPUUtilization',
            Dimensions=[
                {
                    'Name': 'DBClusterIdentifier',
                    'Value': cluster_identifier
                },
            ],
            StartTime=datetime.utcnow() - timedelta(minutes=5),
            EndTime=datetime.utcnow(),
            Period=300,
            Statistics=[
                statistics_type,
            ],
            Unit='Percent'
        )

        if len(response['Datapoints']) == 0:
            print('No data list')
            return False

        # print(response['Datapoints'])
        for point in response['Datapoints']:
            avarage.append(math.ceil((point[statistics_type])))
            print(f"Current Percent CPU: {math.ceil(point['Average'])}%")

        # the current value will be rounded up
        cpuMetric=statistics.mean(avarage)
        if cpuMetric >= autoscaling_target:
            return True
        return False

        