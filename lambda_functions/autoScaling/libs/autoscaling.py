import boto3
import uuid
import logging
import datetime
from datetime import timezone
from utils.timeRange import TimeRange

client = boto3.client('docdb')

class DocumentDB:

  def __init__(self, db_cluster_id, min_capacity=0, max_capacity=15, db_instance_class_default='db.t3.medium', db_instance_class_reziseNight='db.t3.medium'):

    self.db_cluster_id = db_cluster_id
    self.min_capacity = min_capacity
    self.max_capacity = max_capacity
    self.db_instance_class_default = db_instance_class_default
    self.db_instance_class_reziseNight = db_instance_class_reziseNight

    # Needed to know what is the 'primary instance'
    self.db_clusters = client.describe_db_clusters(DBClusterIdentifier=db_cluster_id)

    # Needed to know the instance class
    self.db_instances = client.describe_db_instances(
      Filters=[{
        'Name': 'db-cluster-id',
        'Values': [ db_cluster_id ]
      }]
    )

  def is_modifying(self):
 
    db_instances = self.db_instances.get('DBInstances')
    for db_instance in db_instances:
      print(db_instance.get('DBInstanceIdentifier'), db_instance.get('DBInstanceStatus'))
      if db_instance.get('DBInstanceStatus') != 'available':
        return True

    return False

  def get_replicas_count(self):

    db_instances = self.db_instances.get('DBInstances')
    return len(db_instances[1:])
  
  def get_instance_class(self):
    
    current = TimeRange.time_in_range(datetime.datetime.now(timezone.utc).time())
 
    # If not found primary instance.
    if current: 
      
      return self.db_instance_class_reziseNight
    else:
      return self.db_instance_class_default
    
  def add_replica(self, ignore_status=False):

    print('')
    print('')

    replicas_count = self.get_replicas_count()

    if replicas_count >= self.max_capacity:
      print("Maximum capacity reached.")
      return None

    if self.is_modifying():
      print("Can't create! The instance is modified.")
      return None

    print("Add replica with performance insights...")
    return client.create_db_instance(
      DBClusterIdentifier=self.db_cluster_id,
      DBInstanceIdentifier=f"{self.db_cluster_id}-replica-{uuid.uuid4().hex[0:8]}",
      DBInstanceClass=self.get_instance_class(),
      Engine="docdb",
      EnablePerformanceInsights=True    
    )

  def enablePerformanceInsightsAllInstances(self):

    db_instances = self.db_instances.get('DBInstances')
    print("Trying apply performance insights...")
    for instance in db_instances:
      try:
        client.modify_db_instance(
          DBInstanceIdentifier=instance['DBInstanceIdentifier'],
          DBInstanceClass=self.get_instance_class(),
          EnablePerformanceInsights=True,
          ApplyImmediately=True,
        )
        print("Applied Performance Insights to ", instance['DBInstanceIdentifier'])
      except Exception as error:
        print(error)
        pass

  def add_replica_performanceInsights(self, ignore_status=False):

    replicas_count = self.get_replicas_count()

    if replicas_count >= self.max_capacity:
      logging.error("Maximum capacity reached.")
      return None

    print("Add replica with performance insights...")
    return client.create_db_instance(
      DBClusterIdentifier=self.db_cluster_id,
      DBInstanceIdentifier="%s-%s" % (self.db_cluster_id, uuid.uuid4().hex[0:8]),
      DBInstanceClass=self.get_instance_class(),
      Engine="docdb",
      EnablePerformanceInsights=True  
    )

  def remove_replica(self, ignore_status=False):
    membersOrder = []
    oldInstance = None
    
    if not ignore_status and self.is_modifying():
      print("WARNING: Can't delete! Some instance is being modified.")
      return None

    db_cluster_members = self.db_clusters.get('DBClusters')[0].get('DBClusterMembers')

    for cluster_member in db_cluster_members:
      if not cluster_member.get('IsClusterWriter'):
        membersOrder.append(cluster_member.get('DBInstanceIdentifier'))
    
    for index, member in enumerate(membersOrder[::-1]):
        statusInstance = self.getStatusInstance(member)
        if statusInstance == 'available' and index != len(membersOrder) - 1:
          oldInstance = member
          break

    if oldInstance is None:
      print("WARNING: No instance will be removed or the minimum limit reached!")
      return

    # This role is important for not available the cluster. Minimum replica total read.
    totalMembersAvailable = 0
    for member in membersOrder:
      statusInstance = self.getStatusInstance(member)
      if statusInstance == 'available':
        totalMembersAvailable += 1

    if totalMembersAvailable < self.min_capacity:
      print("WARNING: The minimum available reached!", 'Current: ', totalMembersAvailable, 'min_capacity: ', self.min_capacity)
      return None

    print('WARNING: Removing old replica: ', oldInstance)
    return client.delete_db_instance(
      DBInstanceIdentifier=oldInstance
    )
    # return True


  def getStatusInstance(self, instanceIdentifier):
    if instanceIdentifier is None:
      return None

    instance = client.describe_db_instances(
        DBInstanceIdentifier=instanceIdentifier
    )
    return instance['DBInstances'][0]['DBInstanceStatus']
    