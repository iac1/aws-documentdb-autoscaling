import os
import logging
from libs.autoscaling import DocumentDB
from libs.getMetrics import Metrics

# Environment variables
min_capacity = int(os.environ.get("min_capacity"))
max_capacity = int(os.environ.get("max_capacity"))
cluster_identifier = os.environ.get("cluster_identifier")
db_instance_class_default = os.environ.get('db_instance_class_default')
db_instance_class_reziseNight = os.environ.get('db_instance_class_reziseNight')

docdb = DocumentDB(cluster_identifier, min_capacity, max_capacity, db_instance_class_default, db_instance_class_reziseNight)

def handler(event, context):
  
  print('.')
  print('.')
  print('Running autoScaling...')

  if min_capacity > max_capacity:
    logging.critical("The 'min_capacity' cannot be greater than 'max_capacity'.")
    return None
  
  if docdb.is_modifying():
    print("There is an instance in modification...")
    return None

  isInAlarm = Metrics().get()

  # "In alarm" = scale-out
  if isInAlarm:
    return docdb.add_replica()
  print("AutoScaling is OK!")

# Only local development
if os.environ.get("DEBUG_AUTOSCALING"):
  handler('', '')
