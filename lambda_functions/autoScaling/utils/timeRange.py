import datetime
class TimeRange:
    def time_in_range(currentTime):
        timeCurrentFormated = datetime.time(int(currentTime.strftime("%H")), 0, 0)
        # Define o horário de início às 19:00
        start_time = datetime.time(22, 0)

        # Define o horário final às 09:00 do dia seguinte
        end_time = datetime.time(9, 0)

        # Cria um range de horas, iniciando às 19:00 e terminando às 09:00 do dia seguinte
        if start_time < end_time:
            hours_range = [datetime.time(hour=x) for x in range(start_time.hour, end_time.hour)]
        else:
            hours_range = [datetime.time(hour=x) for x in range(start_time.hour, 24)]
            hours_range += [datetime.time(hour=x) for x in range(0, end_time.hour)]

        # Imprime o range de horas
        for hour in hours_range:
            if hour == timeCurrentFormated:
                return True
        return False
