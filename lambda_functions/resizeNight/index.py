import os
import boto3
import time

client = boto3.client('docdb')

# Environment variables
cluster_identifier = os.environ.get("cluster_identifier")
db_instance_class_reziseNight = os.environ.get("db_instance_class_reziseNight")

resize_nigth_name_primary = os.environ.get("resize_nigth_name_primary")
resize_nigth_name_replica = os.environ.get("resize_nigth_name_replica")

instanceIdentifyNamePrimary=(cluster_identifier + "-" + resize_nigth_name_primary).lower()
instanceIdentifyNameReplica=(cluster_identifier + "-" + resize_nigth_name_replica).lower()


def failover_db_cluster(instancename):
  print("Modify Instance failover", instancename)
  return client.failover_db_cluster(
    DBClusterIdentifier=cluster_identifier,
    TargetDBInstanceIdentifier=instancename
)

def describe_db_instances():
  
   return client.describe_db_instances(
      Filters=[{
        'Name': 'db-cluster-id',
        'Values': [ cluster_identifier ]
      }]
    )

def create_db_instance(instanceIdentifyName):

  try:
    client.create_db_instance(
      DBClusterIdentifier=cluster_identifier,
      DBInstanceIdentifier=instanceIdentifyName,
      DBInstanceClass=db_instance_class_reziseNight,
      Engine="docdb",
      EnablePerformanceInsights=True
    )
  except Exception as error:
    print(error)
    pass

def handler(event, context):

   # Phase 1 creation instance
  primaryInstance=instanceIdentifyNamePrimary
  replicaInstance=instanceIdentifyNameReplica
  create_db_instance(primaryInstance)
  create_db_instance(replicaInstance)

  # Phase 2 Reader cluster for promotion primary instance
  controlLoop=True
  while controlLoop:
    list_db_instances = describe_db_instances()
    for index in list_db_instances["DBInstances"]:
      print('Awaiting for creating and availability of instance: ', primaryInstance)
      if index["DBInstanceIdentifier"] == primaryInstance and index["DBInstanceStatus"] == "available":
        failover_db_cluster(primaryInstance)
        controlLoop=False

    time.sleep(10)

# Only local development
if os.environ.get("DEBUG_AUTOSCALING"):
  handler('', '')    