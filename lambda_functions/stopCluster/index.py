import os
import boto3

client = boto3.client('docdb')

# Environment variables
cluster_identifier = os.environ.get("cluster_identifier")

def handler(event, context):
  return client.stop_db_cluster(
      DBClusterIdentifier=cluster_identifier
  )


# Only local development
if os.environ.get("DEBUG_AUTOSCALING"):
  handler('', '')    