import boto3, os, time

client = boto3.client('docdb')

# Environment variables
cluster_identifier = os.environ.get("cluster_identifier")
db_instance_class_default = os.environ.get("db_instance_class_default")
instanceResizeNightList = {
                            "primary": cluster_identifier + "-" + os.environ.get("resize_nigth_name_primary").lower(),
                            "replica": cluster_identifier + "-" + os.environ.get("resize_nigth_name_replica").lower()
                          }
instanceResizeDayList   = {
                            "primary": cluster_identifier + "-primary".lower(),
                            "replica": cluster_identifier + "-replica".lower()
                          }

def describe_db_clusters():

  return client.describe_db_clusters(
    DBClusterIdentifier=cluster_identifier
  )

def failover_db_cluster(instancename):

  print("Modify Instance failover", instancename)
  return client.failover_db_cluster(
    DBClusterIdentifier = cluster_identifier,
    TargetDBInstanceIdentifier = instancename
  )

def describe_db_instances():
  
   return client.describe_db_instances(
      Filters=[{
        'Name': 'db-cluster-id',
        'Values': [ cluster_identifier ]
      }]
    )

def create_db_instance(instanceIdentifyName):

  try:
    return client.create_db_instance(
      DBClusterIdentifier=cluster_identifier,
      DBInstanceIdentifier=instanceIdentifyName,
      DBInstanceClass=db_instance_class_default,
      Engine="docdb",
      EnablePerformanceInsights=True    
    )
  except Exception as error:
    print(error)
    pass
    

def delete_db_instance(instanceIdentifyName):
  try:
    return client.delete_db_instance(
      DBInstanceIdentifier=instanceIdentifyName
    )
  except Exception:
    pass

def await_failover_instance_primary(instanceName):

  controlLoop=True
  while controlLoop:
    print('Awaiting for promotion of instance primary: ', instanceName)
    list_db_instances = describe_db_clusters()
    for index in list_db_instances['DBClusters'][0]['DBClusterMembers']:
        if index['DBInstanceIdentifier'] == instanceName and index['IsClusterWriter'] == True:
          print(index['DBInstanceIdentifier'])
          controlLoop=False
    
    time.sleep(10)
  return True

def handler(event, context):

  # Phase 1 creation instance
  for instance in instanceResizeDayList:
    # create all instance Day
    create_db_instance(instanceResizeDayList[instance])

  # Phase 2 Reader cluster for promotion primary instance
  controlLoop=True
  while controlLoop:
    list_db_instances = describe_db_instances()
    for index in list_db_instances["DBInstances"]:
      print("Awaiting creation of primary instance", instanceResizeDayList['primary'])
      if index["DBInstanceIdentifier"] == instanceResizeDayList['primary'] and index["DBInstanceStatus"] == "available":
        failover_db_cluster(instanceResizeDayList['primary'])
        # Awaiting for configuration of primary instance
        if await_failover_instance_primary(instanceResizeDayList['primary']):
          # Phase 3 deletion old instance
          for instance in instanceResizeNightList:
            # delete all instance Nigth
            delete_db_instance(instanceResizeNightList[instance])
        controlLoop=False

    time.sleep(10)

# Only local development
if os.environ.get("DEBUG_AUTOSCALING"):
  handler('', '')