import os
import boto3
import time
from libs.getMetrics import GetMetrics

client = boto3.client('docdb')
getMetric                       = GetMetrics()

# Environment variables
cluster_identifier              = os.environ.get("cluster_identifier")
db_instance_class_default       = os.environ.get("db_instance_class_default")
trigger_target                  = os.environ.get("trigger_target")


instanceIdentifyNamePrimary=(cluster_identifier + "-" + 'normalize-primary').lower()
instanceIdentifyNameReplica=(cluster_identifier + "-" + 'normalize-replica').lower()


def failover_db_cluster(instancename):
  print("Modify Instance failover", instancename)
  return client.failover_db_cluster(
    DBClusterIdentifier=cluster_identifier,
    TargetDBInstanceIdentifier=instancename
)

def describe_db_instances():
  
   return client.describe_db_instances(
      Filters=[{
        'Name': 'db-cluster-id',
        'Values': [ cluster_identifier ]
      }]
    )

def create_db_instance(instanceIdentifyName):

  try:
    client.create_db_instance(
      DBClusterIdentifier=cluster_identifier,
      DBInstanceIdentifier=instanceIdentifyName,
      DBInstanceClass=db_instance_class_default,
      Engine="docdb",
      EnablePerformanceInsights=True
    )
  except Exception as error:
    print(error)
    pass

def isSelfserviceInstance():
  for db_instance in describe_db_instances().get('DBInstances'):
    instanceName = db_instance.get('DBInstanceIdentifier')
    # Verify if exists world
    if "selfservice-vertical" in instanceName:
      return True
  print("There aren't instances created by the selfService.")
  return False

def databaseMetricStatus():
  if int(getMetric.get()) < int(trigger_target):
    return True
  return False

def describe_db_instances():
  return client.describe_db_instances(
    Filters=[{
      'Name': 'db-cluster-id',
      'Values': [ cluster_identifier ]
    }]
  )

def is_modifying():
  db_instances = describe_db_instances()
  
  for db_instance in db_instances.get('DBInstances'):
    if db_instance.get('DBInstanceStatus') != 'available':
      print(db_instance.get('DBInstanceIdentifier'), 'status', db_instance.get('DBInstanceStatus'))
      return False
  return True

def validations():
  if isSelfserviceInstance() and is_modifying() and databaseMetricStatus():
    return True
  return False


def handler(event, context):
  
  # phase validation to follow. 
  # Stopped if False return
  # Stopped if database Metric < trigger_target
  # Stopped if not exists instance with name 'selfservice-vertical' 
  if not validations():
    print("Normalization stopped! No necessary requirement.")
    return
  
   # Phase 1 creation instance
  primaryInstance=instanceIdentifyNamePrimary
  replicaInstance=instanceIdentifyNameReplica
  create_db_instance(primaryInstance)
  create_db_instance(replicaInstance)

  # Phase 2 Reader cluster for promotion primary instance
  controlLoop=True
  while controlLoop:
    list_db_instances = describe_db_instances()
    for index in list_db_instances["DBInstances"]:
      print('Awaiting for creating and availability of instance: ', primaryInstance)
      if index["DBInstanceIdentifier"] == primaryInstance and index["DBInstanceStatus"] == "available":
        failover_db_cluster(primaryInstance)
        controlLoop=False

    time.sleep(10)

# Only local development
if os.environ.get("DEBUG_AUTOSCALING"):
  handler('', '')    