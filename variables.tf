variable "cluster_identifier" {
  type        = string
  description = "DocumentDB cluster identifier."
}

variable "name" {
  type        = string
  description = "Resources name."
}

variable "min_capacity" {
  type        = number
  default     = 2
  description = "The minimum capacity."

  # Idiot-proof
  validation {
    condition     = var.min_capacity >= 0
    error_message = "Minimum capacity cannot be lower than 0."
  }

  # Source: https://docs.aws.amazon.com/documentdb/latest/developerguide/how-it-works.html
  validation {
    condition     = var.min_capacity <= 15
    error_message = "DocumentDB does not allow more than 15 replica instances."
  }
}

variable "max_capacity" {
  type        = number
  default     = 15
  description = "The maximum capacity."

  # Source: https://docs.aws.amazon.com/documentdb/latest/developerguide/how-it-works.html
  validation {
    condition     = var.max_capacity <= 15
    error_message = "DocumentDB does not allow more than 15 replica instances."
  }
}

variable "scaling_policy" {
  type = list(object({
    metric_name = string
    target      = number
    statistic   = string
    cooldown    = number
  }))
  description = "The auto-scaling policy."
}

variable "delete_target" {
  default     = 50
  description = "If the value is less than default, then delete instance"
}

variable "db_instance_class_default" {
  type        = string
}

variable "db_instance_class_reziseNight" {
  type        = string
}

variable "resize_nigth_name_primary" {
  type        = string
}

variable "resize_nigth_name_replica" {
  type        = string
}

variable "trigger_target" {
  type        = number
  default     = 50
}

variable "autoscaling_target" {
  type        = number
  default     = 50
}

variable "profile" {
  type        = string
}

variable "region" {
  type        = string
}

variable "aws_account_id" {
  type        = string
}